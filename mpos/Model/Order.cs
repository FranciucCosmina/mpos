using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace mpos.Model
{
  public class Order
  {
    public Order()
    {
      OrderDetailLines = new List<OrderDetail>();
    }

    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int ID { get; set; }
    public DateTime Date { get; set; }

    public double TotalPrice { get; set; }
    public OrderStatus Status { get; set; }

    public virtual List<OrderDetail> OrderDetailLines { get; set; }
  }
}
