using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace mpos.Model
{
  public enum OrderStatus
  {
    NotProcessed,
    Processed,
    Shipped,
    Delivered
  }
}
