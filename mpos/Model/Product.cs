using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace mpos.Model
{
  public class Product
  {
    public Product()
    {
      OrderDetailLines = new List<OrderDetail>();
    }

    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public string Barcode { get; set; }
    public string Name { get; set; }
    public double Price { get; set; }

    public virtual List<OrderDetail> OrderDetailLines { get; set; }
  }
}
