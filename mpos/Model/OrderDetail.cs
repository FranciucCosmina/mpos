using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace mpos.Model
{
  public class OrderDetail
  {
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int OrderLineNumber { get; set; }
    public int Quantity { get; set; }
    public double Price { get; set; }

    public string ProductBarcode { get; set; }
    public virtual Product Product { get; set; }

    public int OrderID { get; set; }
    public virtual Order Order { get; set; }
  }
}
